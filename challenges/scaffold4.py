"""
Level 4: Sending the command string
Evan Johnson

The command string has been removed.

For this level, you'll need to send execution through another call to `read`
so that you can store your own command string in a predictable location. The
`.data` section is good for that; you can overwrite existing data there since
the target program is going to be replaced by a shell anyway.

I've put the `pop rax` instruction back in the helpful gadget this time to
simplify the exploit, but it looks a little different now. Also, I rearranged
some things to keep you on your toes ;)

`read` takes three arguments. They're documented in the manual page, accessible
through the command `man 2 read` at the command line.

NOTE: When running this script, you may find that it only works if you enable
debug logging or insert `sleep()` calls between sending chunks of data to the
target. This issue arises because there's almost no latency on the connection
to the binary when testing locally. If you were connecting to a remote
challenge server, the network latency might solve this issue for you.
"""

from pwn import *
from time import sleep()

target_binary = "./rop3"

context.binary = ELF(target_binary)
context.log_level = "debug"

# stdin=PTY is important!
proc = process(target_binary, stdin=PTY)

syscall_number = 0x3b # 59: execve

######################
# Build the payload. #
######################

# fill buffer


# Set up and jump to the `read` call.
# CAREFUL: the gadget is different on this level



# The read call won't mess up the stack. However, depending on whether you
# jump to the `call read` instruction in main or to the `read` function itself,
# there may or may not be an instruction between it and the next return that
# does. Pad if needed to account for that, or choose where to jump carefully
# to minimize payload size.




# Stage 2: execve syscall
# Remember to use the right address for your command string!



# put the pieces together
payload = ??

#######################
# Exploit the binary! #
#######################

# receive prompt
proc.recv()

## uncomment the following line to debug in GDB:
# gdb.attach(proc)

# Send the payload to exploit the target program's vulnerability.
# Remember to send your command string separately!
??