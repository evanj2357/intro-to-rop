"""
# Level 0: call, return, and pwntools
Evan Johnson

Welcome to the warm-up! This isn't a ROP challenge yet, but it'll help get you
thinking about assembly instructions in a ROP-like way.

This is also a good chance to demonstrate a few Pwntools utilities. The script
below uses the Pwntools library to print the disassembly of an interesting
function in the target binary. Even on relatively simple levels, Pwntools can be
extremely helpful and a great time-saver, so don't wait to start learning. The
library is documented at: https://docs.pwntools.com/en/latest/

Run the "rop0" program in a debugger using the appropriate command (e.g. `gdb rop0`
or `r2 -d rop0`). What does it do? (Feel free to read ahead for the answer, but
be sure to have a look at the assembly code. The answer is in a separate comment
near the bottom of this file.)

## x86-64 instructions used in the Level 0 binary

For your convenience, here's a simplified summary below describing what each
instruction does. You may also wish to consult resources and manuals from 
[Intel](https://software.intel.com/content/www/us/en/develop/articles/introduction-to-x64-assembly.html)
and [AMD](https://www.amd.com/system/files/TechDocs/40332.pdf).

### push _reg/imm_

(_reg_ register, _imm_ = an immediate value, like `0x01` or `0x7ffffff800`)

- subtract 8 from the value in RSP

- treating the value in RSP as a memory address, store the value of _reg_
  at that address

### pop _reg_

Inverse of `push`.

- treating the value in RSP as a memory address, copy the value at that
  address into _reg_

- add 8 to RSP

### call _address/label_

- `push RIP`: push the value in RIP, which is address of the next instruction.
  (see the description of `push` above)

- store _address_ (or the address of _label_) in RIP

Setting RIP to the given address causes execution to jump there.

### ret

- equivalent to `pop RIP`

### xadd _dest_, _src_

Exchange and add _dest_ and _src_. The value of _dest_ will be stored in _src_,
and their sum will be stored in _dest_.

### cmp _dest_, _src_

Compare _dest_ and _src_. This instruction sets flags as if executing
`sub dest, src`, but does not modify the operands.

If _dest_ is less than _src_, the sign flag (SF) will be set; otherwise it
will not be set. `cmp` also modifies other flags; reading about this is left
as an exercise for the reader.

### cmovge _dest_, _src_

Copy data from _src_ to _dest_ if the sign flag (SF) is not set.

### cmovl _dest_, _src_

Copy data from _src_ to _dest_ if the sign flag (SF) is set.
"""

## it's traditional to do:
# from pwn import *
## but this is more clear:
from pwn import ELF, context, disasm

target = ELF("./rop0")

# the `context` object provides a way to configure how pwntools interprets data
context.arch = "amd64"

# look up the address of a symbol by name
addr_of_unusual_function = target.symbols['unusual_function'] 

# I figured this out so you don't have to
function_length = 57

# read starting at the address pwntools found for us
function_bytes = target.read(addr_of_unusual_function, function_length)

# print the disassembly!
print(disasm(function_bytes))

"""
# Solution

The code implements a loop using a trick known as 'push-ret'. This trick jumps
execution to a specified address (just like the `jmp` instruction) in a way that
often confuses decompilers that assume `ret` is used according to convention.
The conditional moves select which address to push before the
return instruction, and that address is where the return jumps to. This is
closely related to ROP and how it got its name: "return-oriented programming."

ROP is the art of creatively (mis)using the `ret` (return) instruction in
existing programs by exploiting buffer overflows (or other out-of-bounds writes)
on the stack. Like this example program, it relies on breaking or ignoring
conventions and the intent behind instructions and blocks of code. The literal
behavior of a sequence of instructions is what matters.

In the next level, you'll exploit a vulnerable program by overwriting an address
that a `ret` instruction will later pop off the stack and supplying additional
data for other instructions to pop into registers.

# Afterword: The Stack FAQ

## The stack?

- In a conventional program, data associated with procedure/function calls is
  placed on "top" of the stack, and popped off the stack when the procedure or
  function returns. This is a convenient way to handle scoping of nested calls.

- By convention, the program's stack grows downward, from high memory addresses
  toward low memory addresses. (Note: writing to a block of memory typically works
  in the opposite way, starting at the lowest address and moving "up".)

- At the end of the day, the stack is just writeable memory that's used in a
  particular way by convention. The `push`, `pop`, `call`, and `ret` instructions
  are implemented in ways that work well with that convention, but the CPU does
  not enforce this convention.

## What about the base pointer?

- The base pointer is a lie.

- More accurately, the base pointer is redundant, so it is typically omitted by
  the `clang` and `gcc` compilers at optimation levels 1 and higher (`-O1` and
  above).

- Even when the base pointer is used, the `call` and `ret` instructions don't
  touch it. The compiler emits `push rbp` and `pop rbp` instructions as needed.

- `rbp` is a general-purpose register.
"""