"""
Level 2: syscall instructions
Evan Johnson

This level is similar to the last one, except this time there's not a convenient
`execve()` call and the binary is statically linked. While there may not be a 
direct call to `execve()` there are now some `syscall` instructions you can use
to accomplish the same effect. Look for them in some of the library functions.

Luckily, the arguments for an `execve` syscall are the same as for the function
call in the previous level, with a slight twist: the `syscall` function is
used for _all_ system calls. The system call to be invoked is determined by a
value passed in RAX. So, you will need control of RAX to select the system call
you want.

Notes:
- A handy reference for Linux system call numbers can be found here:
  https://github.com/torvalds/linux/blob/master/arch/x86/entry/syscalls/syscall_64.tbl

- Linux system calls are documented in section 2 of the manual pages. Use
  the command `man 2 [system call]` to pull up a reference at the command
  line.

- Finding a syscall instruction may require some searching. You have
  several options:
  * radare2 has a regex-based search function specifically for ROP gadgets
  * the "search for binary string" or "search for hex string" option in many
    disassemblers can be used to search for the opcode 0f05
  * search for '0f05' in a hexdump (inconvenient, not recommended)
  * programmatically identify a syscall and its address using the string
    returned by pwntools' `disasm()`
"""

from pwn import *

target_binary = "./rop2"

context.binary = ELF(target_binary)
context.log_level = "debug"

# stdin=PTY is important!
proc = process(target_binary, stdin=PTY)

syscall_number = 0x3b # 59: execve

######################
# Build the payload. #
######################

# (I find it helpful to give each part of my payload a meaningful name, then
# concatenate them all later. The pattern scaffold1 followed is also valid; feel
# free to follow either or develop your own style.)

# padding
padding = ??

# addr of helpful ROP gadget to set up syscall
gadget_addr = ??

# register values in pop order
rax_val = p64(??)
...

# addr of syscall
syscall_addr = ??

# put the pieces together
payload = ??

#######################
# Exploit the binary! #
#######################

# receive prompt
proc.recv()

## uncomment the following line to debug in GDB:
# gdb.attach(proc)

# Send the payload to exploit the target program's vulnerability.
# Remember to set it to interactive mode at the end so you can use the shell!
??