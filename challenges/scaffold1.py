"""
Level 1: First ROP
Evan Johnson

It's time to ROP!

Your goal is to replace the target program with an interactive shell.

The basic principle of return-oriented programming (ROP) is to overwrite
a return address on the stack to jump execution to existing instructions
of your choosing. Using this technique, you can piece together small
pieces of existing code ("gadgets") and existing functions to create your
own program. Replacing the target program with a shell is a common use
case.

To get a shell, you'll want to set up a function call. To do this on x86-64,
you'll need control of some registers, since that is how function arguments
are typically passed (by convention). However, you can only write directly
to the stack.

One of the simplest types of gadget to get your stack data into registers
is a sequence of `pop` instructions followed by a `ret`. I've built one
such gadget into the `rop1` binary to help you get started.

Your payload to exploit the vulnerability in `rop1` should cause three
things to happen:

- jump to the gadget (using the return from `unsafe_input_function`)

- gadget sets up arguments in registers

- jump to `execve()` call

Once written on the stack, it should look something like this:

    (high address)  [address of target call]        # second place to jump to
                    [values to pop into registers]
                    [address of pop-ret gadget]     # first place to jump to
    (low address)   [padding]

Remember that the stack grows down on typical *nix and Windows systems,
so values at lower addresses will be popped first. Also remember that
x86-64 is a little-endian architecture!

You will need to use a disassembler to find the addresses you need for
your payload. This binary already has an `execve` call and the string
"/bin/sh" built-in for you convenience; in the future, you'll need to
find ways to set these things up using your ROP payloads.

Additional notes:

- Python uses the syntax \\xHH for byte values in bytestrings. b'\x00\x01'
  is a null byte followed by the value 1.

- By convention, the first, second, and third arguments to functions are
  passed in RDI, RSI, and RDX, respectively.

- In this case, the compiler used a `push` instruction to make room on the stack
  for an 8-byte buffer instead of explicitly subtracting 8 from RSP.

- `execve` is documented on Linux under the `exec` family of functions.
  Run `man 3 exec` to access the documentation page.

- The `argv` and `envp` arguments to the `execve` call can be null. If
  they are not, the program will attempt to dereference them.

Write code below to construct a payload and run this script with the
command `python scaffold0.py` to solve the challenge. A scaffold and hints
have been provided for convenience.
"""

from pwn import ELF, context, process, p64, PTY, gdb

target_binary = "./rop1"

context.binary = ELF(target_binary)

# print hexdumps of bytes sent and received
context.log_level = "debug"

# stdin=PTY is important!
proc = process(target_binary, stdin=PTY)

## You can write a payload by hand like this:
# payload = b'your payload here\xf0\x70\x40\x00\x00\x00and so on...'

## Or you can use handy helper functions from pwntools for readability:
padding_length = ??
payload = b'a'*padding_length

# append address to return to from vulnerable function (address of gadget)
payload += p64(??) # p64 generates bytes representing a given number as a 64-bit address

# next, the values to go in registers, in the order they'll be popped
payload += ??
...

# fgets wants a newline
payload += b'\n'

# receive prompt
proc.recv()

## if you want to debug your payload in gdb, uncomment the following line:
# gdb.attach(proc)

# send the payload to the target (note: payload must be bytes or a bytes-like object)
proc.send(payload)

# Note: you may need to hit 'enter' to continue the program. Once you have
# a shell, your should see two '$' prompts on the same line (one from pwntools,
# and one from the shell).
proc.interactive()
