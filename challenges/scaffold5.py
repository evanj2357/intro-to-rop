"""
Level 5: Multiple payloads required
Evan Johnson

`pop rax` instruction is gone again, and the program reads less data initially.
That's meant to make setting up an `execve` system call difficult with a single
payload. However, there's nothing stopping you from calling the vulnerable function
at the end of a payload to get a second chance!

Oh, and by the way, the return from the helpful gadget looks a bit different now.

NOTE: When running this script, you may find that it only works if you enable
debug logging or insert `sleep()` calls between sending chunks of data to the
target. This issue arises because there's almost no latency on the connection
to the binary when testing locally. If you were connecting to a remote
challenge server, the network latency might solve this issue for you.
"""

from pwn import *
from time import sleep()

target_binary = "./rop5"

context.binary = ELF(target_binary)
context.log_level = "debug"

# stdin=PTY is important!
proc = process(target_binary, stdin=PTY)

shell_cmd_string = b'/bin/sh\0'
syscall_number = 0x3b # execve


############################
# Build the first payload. #
############################


# use the gadget to call `read`
# CAREFUL: the gadget is different on this level


# return to unsafe_input_function for second payload


# we only have 0x40 bytes to work with
print(hex(len(first_payload)))
assert(len(first_payload) <= 0x40)

#############################
# Build the second payload. #
#############################



# Remember, if you're using the return value of `read` to control rax, the
# length of this payload must be exact. Pad as needed.


#######################
# Exploit the binary! #
#######################

## uncomment the following line to debug in GDB:
# gdb.attach(proc)

# receive and send data to exploit the target program's vulnerability
# remember to set it to interactive mode at the end so you can use the shell!
proc.recv()

proc.send(first_payload)

proc.send(??)

...