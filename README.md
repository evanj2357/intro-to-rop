# Introduction to ROP

Evan Johnson

*Disclaimer: This project is designed to teach students about a class of*
*computer security exploit, for use in an ethical manner.*
*You, the reader, are solely responsible for seeking out information about*
*relevant laws in your jurisdiction, and for ensuring that you have permission*
*to practice these skills on whichever computer system your are using.*

## About the challenges

This project is my best attempt at creating a set of introductory exercises to
learn the basics of return-oriented programming (ROP) exploits. All challenge
levels consist of a 64-bit ELF binary and an associated scaffold script in the
`challenges` directory. Each scaffold script, except for `scaffold0.py`,
provides the basic structure of a `pwntools`-based script to successfully
exploit the associated binary.

The comments in the scripts describe what is needed to "solve" the levels. The
early challenges have extensive comments including some important background
information. Later challenges expect you to either recall or reference earlier
challenges, as well as manuals and documentation referenced in the level
descriptions.

All levels are considered works-in-progress. Feedback is greatly appreciated.

### Pre-requisites

I wrote these levels to bridge the gap between a buffer overflow overwriting a
single variable or return address and development of ROP chains for CTF
challenges.

Pre-requisites for these levels include:

- a Linux system or sufficiently similar environment (i.e. MinGW) to run the
  binaries

- some familiarity with Linux command line use

- some experience writing and debugging code in a Linux environment on a
  x86-64 system

- an understanding of pointers in C

- knowledge of stack frames and how return addresses are stored on the stack
  (the file `notes-on-the-stack.md` contains examples to refresh your memory if
  you need)

- prior exposure to the general principle of stack-based buffer overflows

- willingness to consult reference manuals (but not familiarity with every
  relevant manual)

- have some experience with Python or access to resources to learn from

As far as depth of understanding of the stack, pointers, etc. goes, my intent
is that if you can solve the Chapter 3.7 challenges at https://cs201.oregonctf.org/,
these levels should feel like a possible, although not necessarily easy, next
step. (Note that this is intent, and not necessarily reality. Please let me know
if I've failed you on this, and how.)

### What is return-oriented programming (ROP)?

ROP is a technique for exploiting buffer overflows or arbitrary writes on the
stack, even when the stack is non-executable. It accomplishes this by using code
that already exists in the program. Since the program's own code _must_ be
executable, ROP bypasses data execution prevention. However, other mitigations
(such as stack canaries, position-independent code, and memory safety mechanisms
of languages like Rust) do help against ROP.

### Getting started

1. Clone this project (`git clone [project url]`)

2. Ensure Python is installed, as well as PIP

3. (optional, for Python virtual environment) `pip install virtualenv`

4. (optional, for Python virtual environment) in the project directory, `virtualenv env; source ./env/bin/activate`

5. install pwntools with the command `pip install pwntools`

6. Install any other utilities, such as `gdb` and `radare2`, if needed/wanted.

7. Navigate to the `challenges` directory, read `scaffold0.py`, run it, and run `rop0`

8. Good luck, and have fun!

## Helpful resources

### Tools

Apart from Python and Pwntools, on which the scaffold scripts depend, I've
found the following tools helpful:

#### Debuggers

- gdb

- radare2

#### Disassemblers and decompilers

- radare2 and Cutter

- Ghidra

- objdump

- Binary Ninja (costs money)

#### Utilities

- grep (text search in files and output of other utilities)

- echo (can be used with -ne to send bytes from the command line manually)

### Reference materials

These are references and resources that I have found helpful while writing and
attempting to solve these challenges myself:

- Linux manual pages: `man [topic]` at the command line, or https://man7.org/linux/man-pages/index.html
on the internet

- Pwntools documentation: https://docs.pwntools.com/en/latest/

- x86-64 reference manuals: [Intel](https://software.intel.com/content/www/us/en/develop/articles/introduction-to-x64-assembly.html)
and [AMD](https://www.amd.com/system/files/TechDocs/40332.pdf)

In the Intel 64 software developer's manual, Volume 2 documents instructions
alphabetically by the mnemonic used in assembly programming. In the AMD manual,
Volume 3 contains similar documentation for the general-purpose instructions.
Both manuals document the x86-64, aka AMD64, aka x64, instruction set, although
they may also document features specific to each company's respective products.
Each manual also documents its reference page formats and abbreviations used; I
recommend learning to read the manuals so you can look things up as needed.

## Dependencies

The scaffold and sample solution scripts depend on Python 3 and the pwntools
library.

On a Linux system with Python 3 and PIP installed, pwntools can be obtained
by running the command `pip install pwntools`. In order to avoid dependency
conflicts with other Python libraries you may have installed, I recommend using
a Python virtual environment.

The C programs depend only on standard C libraries that are included with nearly
all Linux systems and a C compiler. I used the Clang compiler because I find its
error messages more readable than GCC's, and usability is important.

The binaries were linked against the version of the GNU C Library distributed
with Ubuntu 20.03 as the package `libc6` (2.31-0ubuntu9.2).

## Licensing

### My code

The provided C, Python, and x86-64 assembler source code files are licensed
under the permissive MIT license. I selected it to make clear that you may use
my creations here for any lawful pupose and to serve as a reminder to credit the
original creators of things.

### Precompiled binaries and the GNU C Library

The challenge binaries use code from the GNU C Library that is licensed under
the GNU Lesser General Public License. I can provide a copy of the source code
on request. You can also obtain a copy directly from the source (pun intended)
with the commands:
```
git clone https://sourceware.org/git/glibc.git
cd glibc
git checkout release/2.31/master
```

The text of the GNU C Library licenses document can be found in `LICENSE-GLIBC.txt`.

A copy of the LGPL can be found in `LGPL-3.0.txt`.