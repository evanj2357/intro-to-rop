# Notes on the Stack

The "stack" is a segment of memory that's conventionally used for a stack data
structure tracking function activation records. Each function call results in a
corresponding "frame" being added to the stack. The frame stores the address of
the instruction to return to as well as any local variables that aren't entirely
kept in registers. It may also be used to store other data related to the call
as well (such as saving a base pointer, if base pointers are used).

The `call` and `ret` instructions help implement this by implicitly operating on
stack memory and the RSP ("stack pointer") register. The following example code
illustrates the behavior of `call` and `ret` with the stack and an input that
resembles a ROP payload.

## Excerpt from a hypothetical x86-64 program

_Note: Addresses in this example are hypothetical and only approximately_
_reflect what you might see in real code._

```
fn_1:
0x401000 sub    rsp, 0x20
0x401004 mov    rdi, 0x10(rsp) // buffer1
0x401008 call   fn_2
0x40100d add    rsp, 0x20
0x401011 ret

fn_2:
0x401018 sub    rsp, 0x10
0x40101c push   rdi
0x40101d mov    rdi, 0x08(rsp) // temp_buffer
// read way too many bytes into temp_buffer (pointed by RDI)
...
// copy data from temp_buffer into buffer1
...
0x401040 add    rsp, 0x10
0x201044 ret
```

Here's what execution might look like with a ROP payload that jumps through an
extra `ret` instruction and repeats the read-and-copy code in `fn_2`:

### Initial state

| _Registers_ | |
| ---: | -----------: |
| rdi | 0x7ffffe040 |
| rsp | 0x7ffffe030 |
| rip |    0x401008 |

_Note: The return address from `fn_1` is at `0x7ffffe050`_

| _Stack_ | |
| -----------: | ------------------: |
| 0x7ffffe050 |           0x400080 |
| 0x7ffffe048 | 0x0000000000000000 |
| 0x7ffffe040 | 0x0000000000000000 |
| 0x7ffffe038 | 0x0000000000000000 |
| 0x7ffffe030 | 0x0000000000000000 |

```
        fn_1:
        0x401000 sub    rsp, 0x20
        0x401004 mov    rdi, 0x10(rsp) // buffer1
rip --> 0x401008 call   fn_2
        0x40100d add    rsp, 0x20
        0x401011 ret

        fn_2:
        0x401018 sub    rsp, 0x10
        0x40101c push   rdi
        0x40101d mov    rdi, 0x08(rsp) // temp_buffer
        // read way too many bytes into temp_buffer (pointed by RDI)
        ...
        // copy data from temp_buffer into buffer1
        ...
        0x401040 add    rsp, 0x10
        0x201044 ret
```

### `fn_2` called

| _Registers_ | |
| ---: | -----------: |
| rdi | 0x7ffffe040 |
| rsp | 0x7ffffe028 |
| rip |    0x401018 |


| _Stack_ | |
| -----------: | ------------------: |
| 0x7ffffe050 |           0x400080 |
| 0x7ffffe048 | 0x0000000000000000 |
| 0x7ffffe040 | 0x0000000000000000 |
| 0x7ffffe038 | 0x0000000000000000 |
| 0x7ffffe030 | 0x0000000000000000 |
| 0x7ffffe028 |       (A) 0x40100d |


```
        fn_1:
        0x401000 sub    rsp, 0x20
        0x401004 mov    rdi, 0x10(rsp) // buffer1
        0x401008 call   fn_2
    (A) 0x40100d add    rsp, 0x20
        0x401011 ret

        fn_2:
rip --> 0x401018 sub    rsp, 0x10
        0x40101c push   rdi
        0x40101d mov    rdi, 0x08(rsp) // temp_buffer
        // read way too many bytes into temp_buffer (pointed by RDI)
        ...
        // copy data from temp_buffer into buffer1
        ...
        0x401040 add    rsp, 0x10
        0x201044 ret
```


### Stack adjusted for local `temp_buffer`

| _Registers_ | |
| ---: | -----------: |
| rdi | 0x7ffffe040 |
| rsp | 0x7ffffe018 |
| rip |    0x40101c |


| _Stack_ | |
| -----------: | ------------------: |
| 0x7ffffe050 |           0x400080 |
| 0x7ffffe048 | 0x0000000000000000 |
| 0x7ffffe040 | 0x0000000000000000 |
| 0x7ffffe038 | 0x0000000000000000 |
| 0x7ffffe030 | 0x0000000000000000 |
| 0x7ffffe028 |       (A) 0x40100d |
| 0x7ffffe020 | 0x0000000000000000 |
| 0x7ffffe018 | 0x0000000000000000 |


```
        fn_1:
        0x401000 sub    rsp, 0x20
        0x401004 mov    rdi, 0x10(rsp) // buffer1
        0x401008 call   fn_2
        0x40100d add    rsp, 0x20
        0x401011 ret

        fn_2:
        0x401018 sub    rsp, 0x10
rip --> 0x40101c push   rdi
        0x40101d mov    rdi, 0x08(rsp) // temp_buffer
        // read way too many bytes into temp_buffer (pointed by RDI)
        ...
        // copy data from temp_buffer into buffer1
        ...
        0x401040 add    rsp, 0x10
        0x201044 ret
```

### Saved pointer to `buffer1`

| _Registers_ | |
| ---: | -----------: |
| rdi | 0x7ffffe040 |
| rsp | 0x7ffffe010 |
| rip |    0x40101d |


| _Stack_ | |
| -----------: | ------------------: |
| 0x7ffffe050 |           0x400080 |
| 0x7ffffe048 | 0x0000000000000000 |
| 0x7ffffe040 | 0x0000000000000000 |
| 0x7ffffe038 | 0x0000000000000000 |
| 0x7ffffe030 | 0x0000000000000000 |
| 0x7ffffe028 |       (A) 0x40100d |
| 0x7ffffe020 | 0x0000000000000000 |
| 0x7ffffe018 | 0x0000000000000000 |
| 0x7ffffe010 |        0x7ffffe040 |


```
        fn_1:
        0x401000 sub    rsp, 0x20
        0x401004 mov    rdi, 0x10(rsp) // buffer1
        0x401008 call   fn_2
    (A) 0x40100d add    rsp, 0x20
        0x401011 ret

        fn_2:
        0x401018 sub    rsp, 0x10
        0x40101c push   rdi
rip --> 0x40101d mov    rdi, 0x08(rsp) // temp_buffer
        // read way too many bytes into temp_buffer (pointed by RDI)
        ...
        // copy data from temp_buffer into buffer1
        ...
        0x401040 add    rsp, 0x10
        0x201044 ret
```

### Starting input and copy code

| _Registers_ | |
| ---: | -----------: |
| rdi | 0x7ffffe018 |
| rsp | 0x7ffffe010 |
| rip |    0x401012 |


| _Stack_ | |
| -----------: | ------------------: |
| 0x7ffffe050 |           0x400080 |
| 0x7ffffe048 | 0x0000000000000000 |
| 0x7ffffe040 | 0x0000000000000000 |
| 0x7ffffe038 | 0x0000000000000000 |
| 0x7ffffe030 | 0x0000000000000000 |
| 0x7ffffe028 |       (A) 0x40100d |
| 0x7ffffe020 | 0x0000000000000000 |
| 0x7ffffe018 | 0x0000000000000000 |
| 0x7ffffe010 |        0x7ffffe040 |


```
        fn_1:
        0x401000 sub    rsp, 0x20
        0x401004 mov    rdi, 0x10(rsp) // buffer1
        0x401008 call   fn_2
    (A) 0x40100d add    rsp, 0x20
        0x401011 ret

        fn_2:
        0x401018 sub    rsp, 0x10
        0x40101c push   rdi
        0x40101d mov    rdi, 0x08(rsp) // temp_buffer
rip >   // read way too many bytes into temp_buffer (pointed by RDI)
        ...
        // copy data from temp_buffer into buffer1
        ...
        0x401040 add    rsp, 0x10
        0x201044 ret
```

### `temp_buffer` overflowed, copied to `buffer1`

The overflowing input bytes (in hex) are:

`3131313131313131 3131313131313131 1110400000000000 1c10400000000000 dededededededede cdcdcdcdcdcdcdcd`

Assume the copy stopped at the first null byte (as if using `strcpy`). Note that
_two_ return addresses have been overwritten by this overflowing read and copy,
because both buffers were the same size.

| _Registers_ | |
| ---: | -----------: |
| rdi | 0x7ffffe040 |
| rsp | 0x7ffffe018 |
| rip |    0x401040 |


| _Stack_ | |
| -----------: | ------------------: |
| 0x7ffffe050 |           0x401004 |
| 0x7ffffe048 | 0x3131313131313131 |
| 0x7ffffe040 | 0x3131313131313131 |
| 0x7ffffe038 | 0xdededededededede |
| 0x7ffffe030 |           0x40101c |
| 0x7ffffe028 |           0x401004 |
| 0x7ffffe020 | 0x3131313131313131 |
| 0x7ffffe018 | 0x3131313131313131 |


```
        fn_1:
        0x401000 sub    rsp, 0x20
        0x401004 mov    rdi, 0x10(rsp) // buffer1
        0x401008 call   fn_2
    (A) 0x40100d add    rsp, 0x20
        0x401011 ret

        fn_2:
        0x401018 sub    rsp, 0x10
        0x40101c push   rdi
        0x40101d mov    rdi, 0x08(rsp) // temp_buffer
        // read way too many bytes into temp_buffer (pointed by RDI)
        ...
        // copy data from temp_buffer into buffer1
        // assume there's a POP RDI in here
        ...
rip --> 0x401040 add    rsp, 0x10
        0x201044 ret
```

### Stack readjusted at end of `fn_2`

| _Registers_ | |
| ---: | -----------: |
| rdi | 0x7ffffe040 |
| rsp | 0x7ffffe028 |
| rip |    0x401044 |


| _Stack_ | |
| -----------: | ------------------: |
| 0x7ffffe050 |           0x401004 |
| 0x7ffffe048 | 0x3131313131313131 |
| 0x7ffffe040 | 0x3131313131313131 |
| 0x7ffffe038 | 0xdededededededede |
| 0x7ffffe030 |           0x40101c |
| 0x7ffffe028 |           0x401011 |


```
        fn_1:
        0x401000 sub    rsp, 0x20
        0x401004 mov    rdi, 0x10(rsp) // buffer1
        0x401008 call   fn_2
    (A) 0x40100d add    rsp, 0x20
        0x401011 ret

        fn_2:
        0x401018 sub    rsp, 0x10
        0x40101c push   rdi
        0x40101d mov    rdi, 0x08(rsp) // temp_buffer
        // read way too many bytes into temp_buffer (pointed by RDI)
        ...
        // copy data from temp_buffer into buffer1
        // assume there's a POP RDI in here
        ...
        0x401040 add    rsp, 0x10
rip --> 0x201044 ret
```

### Returned from `fn_2`

Note that we did _not_ return to the return address that was saved by the `call`
instruction--it was overwritten.

| _Registers_ | |
| ---: | -----------: |
| rdi | 0x7ffffe040 |
| rsp | 0x7ffffe030 |
| rip |    0x401011 |


| _Stack_ | |
| -----------: | ------------------: |
| 0x7ffffe050 |           0x401004 |
| 0x7ffffe048 | 0x3131313131313131 |
| 0x7ffffe040 | 0x3131313131313131 |
| 0x7ffffe038 | 0xdededededededede |
| 0x7ffffe030 |           0x40101c |


```
        fn_1:
        0x401000 sub    rsp, 0x20
        0x401004 mov    rdi, 0x10(rsp) // buffer1
        0x401008 call   fn_2
    (A) 0x40100d add    rsp, 0x20
rip --> 0x401011 ret

        fn_2:
        0x401018 sub    rsp, 0x10
        0x40101c push   rdi
        0x40101d mov    rdi, 0x08(rsp) // temp_buffer
        // read way too many bytes into temp_buffer (pointed by RDI)
        ...
        // copy data from temp_buffer into buffer1
        // assume there's a POP RDI in here
        ...
        0x401040 add    rsp, 0x10
        0x201044 ret
```

### "Returned" again!

Note that we did _not_ return to the return address that was saved by the `call`
instruction--it was overwritten.

| _Registers_ | |
| ---: | -----------: |
| rdi | 0x7ffffe040 |
| rsp | 0x7ffffe030 |
| rip |    0x40101c |


| _Stack_ | |
| -----------: | ------------------: |
| 0x7ffffe050 |           0x401004 |
| 0x7ffffe048 | 0x3131313131313131 |
| 0x7ffffe040 | 0x3131313131313131 |
| 0x7ffffe038 | 0xdededededededede |


```
        fn_1:
        0x401000 sub    rsp, 0x20
        0x401004 mov    rdi, 0x10(rsp) // buffer1
        0x401008 call   fn_2
        0x40100d add    rsp, 0x20
        0x401011 ret

        fn_2:
        0x401018 sub    rsp, 0x10
rip --> 0x40101c push   rdi
        0x40101d mov    rdi, 0x08(rsp) // temp_buffer
        // read way too many bytes into temp_buffer (pointed by RDI)
        ...
        // copy data from temp_buffer into buffer1
        // assume there's a POP RDI in here
        ...
        0x401040 add    rsp, 0x10
        0x201044 ret
```
