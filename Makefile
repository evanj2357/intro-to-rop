CC=clang
CFLAGS=-z noexecstack -fno-stack-protector -fomit-frame-pointer -fno-pie -g -O0
STATIC_CHALLS=rop2 rop3 rop4 rop5
DEST=./challenges

all: warm-up rop1 $(STATIC_CHALLS)

warm-up: src/rop0.c src/warm-up.s
	$(CC) $(CFLAGS) -masm=intel $^ -o $(DEST)/rop0

rop1: src/rop1.c
	$(CC) $(CFLAGS) $^ -o $(DEST)/rop1

$(STATIC_CHALLS): %: src/%.c
	$(CC) $(CFLAGS) -static -mno-mpx $^ -o $(DEST)/$@

clean:
	rm $(DEST)/rop*