// rop6.c
// Evan Johnson
//
// An introductory ROP challenge that doesn't have the necessary syscall and
// command string built in. This is more like "easy" ROPs I've seen in CTFs.
//
// No helpful gadget function this time.

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

char instructions[] = "ROP it!\n(hint: you may need to set up your own fgets() call before going for the shell)\n";

int unsafe_input_function(void);

int main(int argc, char** argv)
{
    printf("%s", instructions);

    unsafe_input_function();

    return 0;
}

int unsafe_input_function(void)
{
    // 8-byte buffer
    char buffer[0x8];

    // Buffer overflow vulnerability here: `read` reads 128 chars of input, but
    // the buffer is only 8 chars long. This allows the user to overwrite the
    // saved return address on the stack and also put several more pointers
    // on the stack.
    int count = read(0, buffer, 0x80);

    // Returning the number of chars read gives the attacker control over
    // eax/rax by varying input length.
    return count;
}