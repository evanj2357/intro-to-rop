
.global unusual_function

unusual_function:
    xor rcx, rcx
    mov rdx, rdi
    mov rax, 0x1
    mov rbx, 0x0
    lea r9, finish
    call check
    xadd rax, rbx
    inc rcx
check:
    cmp rcx, rdx
    cmovl rbp, qword ptr [rsp]
    cmovge rbp, r9
    push rbp
    ret
finish:
    pop r8
    ret