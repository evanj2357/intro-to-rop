/* Level 0: `call` and `ret`
 * Evan Johnson
 *
 * This isn't ROP yet--just a little reverse-engineering fun to remind yourself
 * of the mechanics of `call`, `ret`, and the stack on x86-64.
 *
 * The magic is in warm-up.s
 */


#include <stdio.h>

extern unsigned long long unusual_function(unsigned int n);

int main(int argc, char** argv)
{
    printf("%llu\n", unusual_function(20));
    return 0;
}