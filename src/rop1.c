// rop1.c
// Evan Johnson
//
// Commented C source for a generic entry-level ROP challenge, intended to help
// students and CTF players learn the basics of return-oriented programming.

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

// I've "accidentally" included a string that might help with ROP at the end of
// the instructions to simplify things.
const char instructions[] = "Craft an input to overflow the 8-byte buffer and use return-oriented programming to get a shell!\0/bin/sh";
const char* program_to_run = "/bin/whoami";
char* exec_args[] = {"whoami", NULL};
// __attribute__((used))
// char* rop_exec_args[] = {"sh", "-i", NULL};
// char** saved_envp;

void rop_gadget_here(void);
void unsafe_input_function(void);
void show_data(char*, int);

int main(int argc, char** argv, char** envp)
{
    printf("Practicing ROP as: ");
    fflush(stdout);
    int pid = fork();
    if (!pid)
    {
        execve("/bin/whoami", exec_args, envp);

        exit(0);
    }
    sleep(1);

    // print the instructions
    printf("%96s\n:", instructions);

    unsafe_input_function();
    exit(0);
}

// The ROP gadget here is:
//
//   pop rdi
//   pop rsi
//   pop rdx
//   ret
//
// Control over rdi is all that's needed to point the execve() call to the
// desired command string.
//
// Notes for ROP:
//   - avoid returning to the push instruction, as that will preserve the
//     existing value in rdi
__attribute__((used))
void rop_gadget_here(void)
{
    __asm__ volatile (
        "push %%rdx\n"
        "push %%rsi\n"
        "push %%rdi\n"
        "pop %%rdi\n"
        "pop %%rsi\n"
        "pop %%rdx\n"
        "ret"
        :
        :
        : "%rdi", "%rsi", "%rdx"
    );
}

void unsafe_input_function(void)
{
    // 8-byte buffer
    char buffer[8];

    // Buffer overflow vulnerability here: fgets reads 128 chars of input, but
    // the buffer is only 8 chars long. This allows the user to overwrite the
    // saved return address on the stack and also put several more pointers
    // on the stack.
    fgets(buffer, 0x80, stdin);

    return;
}