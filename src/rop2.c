// rop2.c
// Evan Johnson
//
// An introductory ROP challenge that doesn't have the `execve` call built-in.
// Compile with static linking for the syscall instructions.

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

char instructions[] = "ROP it!\n\0/bin/sh";

int unsafe_input_function(void);
void rop_gadget_here(void);

int main(int argc, char** argv)
{
    printf("%s", instructions);

    unsafe_input_function();

    return 0;
}

int unsafe_input_function(void)
{
    // 8-byte buffer
    char buffer[0x08];

    // Buffer overflow vulnerability here: `read` reads 128 chars of input, but
    // the buffer is only 8 chars long. This allows the user to overwrite the
    // saved return address on the stack and also put several more pointers
    // on the stack.
    int count = read(0, (char*)buffer, 0x80);

    // Returning the number of chars read gives the attacker control over
    // eax/rax by varying input length.
    return count;
}

__attribute__((used))
void rop_gadget_here(void)
{
    __asm__ volatile (
        "push %rdx\n"
        "push %rsi\n"
        "push %rdi\n"
        "push %rax\n"
        "pop %rax\n"
        "pop %rdi\n"
        "pop %rsi\n"
        "pop %rdx\n"
        "ret\n"
    );
}