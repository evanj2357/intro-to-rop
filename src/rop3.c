// rop3.c
// Evan Johnson
//
// An introductory ROP challenge that doesn't have the necessary syscall and
// command string built in. This is more like "easy" ROPs I've seen in CTFs.

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

char instructions[] = "ROP it!\n\0/bin/sh";

int unsafe_input_function(void);
void rop_gadget_here(void);

int main(int argc, char** argv)
{
    printf("%s", instructions);

    unsafe_input_function();

    return 0;
}

int unsafe_input_function(void)
{
    // 8-byte buffer
    char buffer[0x08];

    // Buffer overflow vulnerability here: `read` reads 128 chars of input, but
    // the buffer is only 8 chars long. This allows the user to overwrite the
    // saved return address on the stack and also put several more pointers
    // on the stack.
    int count = read(0, (char*)buffer, 0x80);

    // Returning the number of chars read gives the attacker control over
    // eax/rax by varying input length.
    return count;
}

__attribute__((used))
void rop_gadget_here(void)
{
    __asm__ volatile (
        "push %rdi\n"
        "push %rsi\n"
        "push %rdx\n"
        "pop %rdx\n"
        "pop %rsi\n"
        "pop %rdi\n"
        "ret\n"
    );
}